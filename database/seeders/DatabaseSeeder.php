<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        // user account seeder
        DB::table('users')->insert([
            [
                'username' => 'Admin',
                'email' => 'admin@admin.com',
                'role_id' => 1,
                'password' => Hash::make('asdasd123'),
            ],

            [
                'username' => 'Andi',
                'email' => 'andi@andi.com',
                'role_id' => 2,
                'password' => Hash::make('12345'),
            ],

            [
                'username' => 'Budi',
                'email' => 'budi@budi.com',
                'role_id' => 2,
                'password' => Hash::make('67890'),
            ],

            [
                'username' => 'Caca',
                'email' => 'caca@caca.com',
                'role_id' => 2,
                'password' => Hash::make('abcde'),
            ],

            [
                'username' => 'Deni',
                'email' => 'deni@deni.com',
                'role_id' => 2,
                'password' => Hash::make('fghij'),
            ],

            [
                'username' => 'Euis',
                'email' => 'euis@euis.com',
                'role_id' => 2,
                'password' => Hash::make('klmno'),
            ],

            [
                'username' => 'Fafa',
                'email' => 'fafa@fafa.com',
                'role_id' => 2,
                'password' => Hash::make('pqrst'),
            ],
        ]);

        //user role seeder
        DB::table('user_roles')->insert([
            [
                'name' => 'Superadmin',
            ],

            [
                'name' => 'User',
            ],
        ]);

        //course seeder
        DB::table('courses')->insert([
            [
                'course' => 'C++',
                'mentor' => 'Ari',
                'title' => 'Dr.',
            ],

            [
                'course' => 'C#',
                'mentor' => 'Ari',
                'title' => 'Dr.',
            ],

            [
                'course' => 'C#',
                'mentor' => 'Ari',
                'title' => 'Dr.',
            ],

            [
                'course' => 'CSS',
                'mentor' => 'Cania',
                'title' => 'S.Kom',
            ],

            [
                'course' => 'HTML',
                'mentor' => 'Cania',
                'title' => 'S.Kom',
            ],

            [
                'course' => 'Javascript',
                'mentor' => 'Cania',
                'title' => 'S.Kom',
            ],

            [
                'course' => 'Phyton',
                'mentor' => 'Barry',
                'title' => 'S.T',
            ],

            [
                'course' => 'Microphyton',
                'mentor' => 'Barry',
                'title' => 'S.T',
            ],

            [
                'course' => 'Java',
                'mentor' => 'Darren',
                'title' => 'M.T',
            ],

            [
                'course' => 'Ruby',
                'mentor' => 'Darren',
                'title' => 'M.T',
            ],
        ]);

        //users & courses
        DB::table('user_courses')->insert([
            [
                'id_user' => 1,
                'id_course' => 1,
            ],

            [
                'id_user' => 1,
                'id_course' => 2,
            ],

            [
                'id_user' => 1,
                'id_course' => 3,
            ],

            [
                'id_user' => 2,
                'id_course' => 4,
            ],

            [
                'id_user' => 2,
                'id_course' => 5,
            ],

            [
                'id_user' => 2,
                'id_course' => 6,
            ],

            [
                'id_user' => 3,
                'id_course' => 7,
            ],

            [
                'id_user' => 3,
                'id_course' => 8,
            ],

            [
                'id_user' => 3,
                'id_course' => 9,
            ],

            [
                'id_user' => 4,
                'id_course' => 1,
            ],

            [
                'id_user' => 4,
                'id_course' => 3,
            ],

            [
                'id_user' => 4,
                'id_course' => 5,
            ],

            [
                'id_user' => 5,
                'id_course' => 2,
            ],

            [
                'id_user' => 5,
                'id_course' => 4,
            ],

            [
                'id_user' => 5,
                'id_course' => 6,
            ],

            [
                'id_user' => 6,
                'id_course' => 7,
            ],

            [
                'id_user' => 6,
                'id_course' => 8,
            ],

            [
                'id_user' => 6,
                'id_course' => 9,
            ],
        ]);
    }
}
