import { alert, reloadTable, requestData, showInvalidMessage } from "../global/global.js"
import { modalDeleteUserCourse, modalUserCourse } from "./menu-usercourse.js"

let tableCourses, content, itemId, isInsert
content = document.querySelector('.content')

tableCourses = $('#datatable-usercourses').DataTable({
        processing: true,
        serverSide: true,
        ajax: "/usercourses",
        lengthMenu: [[25, 50, 100, -1], [25, 50, 100, "All"]],
        columns: [
            {data: 'DT_RowIndex', name: 'id', orderable: false, searchable: false},
            {data: 'id_user', name: 'id_user'},
            {data: 'id_course', name: 'id_course'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        language: {
            lengthMenu: "_MENU_ Data per halaman",
            //zeroRecords: "Data tidak ditemukan",
            info: "Menampilkan halaman _PAGE_ dari _PAGES_",
            //infoEmpty: "Data kosong",
            infoFiltered: "(filter dari _MAX_ total data)",
            search: "Cari :",
    
            oPaginate: {
                sNext: '<i class="bi bi-arrow-right-short"></i>',
                sPrevious: '<i class="bi bi-arrow-left-short"></i>',
                sFirst: 'Pertama',
                sLast: 'Terakhir'
            },
        },
        dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-12'i><'col-12'<'d-flex justify-content-center'p>>>",
        drawCallback: () => {

            //edit data
            const btnEdit = document.querySelectorAll('.edit-data')
            btnEdit.forEach(btn => btn.addEventListener('click', async function (e) {
                this.disabled = true
                isInsert = false
                itemId = this.dataset.id

                const data = {
                    title: 'Edit User Course',
                }

                try {
                    const result = await requestData(`usercourses/${itemId}/edit`, 'GET')
                    data.users = result.users
                    data.courses = result.courses
                    data.course = result.course
    
                    content.insertAdjacentHTML('afterend', modalUserCourse(data))
                    const modalEdit = document.getElementById('modal-usercourse')
                    const modal = new bootstrap.Modal(modalEdit)
                    modal.show()
    
                    submit(isInsert, modal)
    
                    modalEdit.addEventListener('hidden.bs.modal', () => {
                        modalEdit.remove()
                        this.disabled = false
                    })
                } catch (error) {
                    alert('error', 'Gagal', error.message)
                }
            }))

            // Delete data
            const btnDelete = document.querySelectorAll('.delete-data')
            btnDelete.forEach(btn => btn.addEventListener('click', async function () {
                this.disabled = true
                itemId = this.dataset.id

                content.insertAdjacentHTML('afterend', modalDeleteUserCourse())
                const modalDelete = document.getElementById('delete-modal')
                const modal = new bootstrap.Modal(modalDelete)
                modal.show()
                const formDelete = document.querySelector('#form-delete-usercourse')
                formDelete.addEventListener('submit', async (e) => {
                    e.preventDefault()
                    modal.hide()
                    try {
                        const result = await requestData(`/usercourses/delete/${itemId}`, 'DELETE')
                        alert('success', 'Berhasil', result.message)
                    } catch (error) {
                        alert('error', 'Gagal', error.message) 
                    }

                    reloadTable(tableCourses)
                })
                modalDelete.addEventListener('hidden.bs.modal', () => {
                    modalDelete.remove()
                    this.disabled = false
                })
            }))
        }
    })

//tambah course
const btnAdd = document.querySelector('#add-usercourse')
btnAdd.addEventListener('click', async function (e) {
    btnAdd.disabled = true
    isInsert = true

    const data = {
        title: 'Tambah user course'
    }

    try {
        // get data roles
        const result = await requestData('/usercourses/getAll', 'GET')
        data.users = result.users
        data.courses = result.courses

        content.insertAdjacentHTML('afterend', modalUserCourse(data))
        const modalAdd = document.getElementById('modal-usercourse')
        const modal = new bootstrap.Modal(modalAdd)
        modal.show()

        submit(isInsert, modal)
        modalAdd.addEventListener('hidden.bs.modal', () => {
           modalAdd.remove()
           btnAdd.disabled = false
        })
    } catch (error) {
        alert('error', 'Gagal', error.message)
    }
  
})

const submit = (isInsert, modal) => {
    const btnSubmit = document.querySelector('#btn-submit')
    btnSubmit.addEventListener('click', async (e) => {
        e.preventDefault()

        const formData = new FormData()
        const formCourse = document.querySelectorAll('.form-usercourse')

        formCourse.forEach(form => formData.append(form.getAttribute('name'), form.value))

        let url
        
        if (isInsert) {
            url = '/usercourses/store'
        } else {
            url = `/usercourses/update/${itemId}`
            formData.append('_method', 'PATCH')
        }

        try {
            const result = await requestData(url, 'POST', formData)
            alert('success', 'Berhasil', result.message)
            modal.hide()
            reloadTable(tableCourses)
        } catch (error) {
            // error validation
            if (error.status == 422) {
                showInvalidMessage('form-usercourse', error.validation)
                return false
            }
 
            alert('error', 'Gagal', error.message)
        }
    })

}