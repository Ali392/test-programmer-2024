<?php

namespace App\Http\Controllers;

use App\Models\UserCourse;
use App\Models\User;
use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class UserCoursesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (request()->ajax()) {
            $data = UserCourse::with('user', 'course')->orderBy('id_user', 'asc')->get();
            return DataTables::of($data)
              ->addIndexColumn()
              ->addColumn('action', function ($course) {
                    return '
                        <div class="d-flex">
                          <button class="btn btn-warning btn-sm mx-1 edit-data" data-id="' . $course->id . '" type="button"><i class="bi bi-pencil"></i></button>
                          <button class="btn btn-danger btn-sm delete-data" data-id="' . $course->id . '" type="button"><i class="bi bi-trash"></i></button>
                        </div>
                        ';
                
              })
              ->editColumn('id_user', function ($course) {
                return $course->user->username ?? '';
              })
              ->editColumn('id_course', function ($course) {
                return $course->course->course ?? '';
              })
              ->rawColumns(['action'])
              ->make('true');
        }

        return view('dashboard.usercourses.index');
    }

    public function store(Request $request)
    {
        $rules = [
            'id_user' => 'required',
            'id_course' => 'required',
        ];

        $messages = [
            'required' => ':attribute harus diisi',
        ];

        $aliases = [
            'id_user' => 'User',
            'id_course' => 'Course',
        ];

        $validator = Validator::make($request->all(), $rules, $messages, $aliases);

        if ($validator->fails()) {
            return ['status' => 422, 'validation' => $validator->getMessageBag()];
        }

        try {
            //check data sudah ada
            $check = UserCourse::where('id_user', $request->id_user)->where('id_course', $request->id_course)->get();
            if($check->count() != 0)
            {
                return ['status' => 500, 'message' => 'Data gagal ditambahkan, user dan kursus sudah ada!'];
            }

            $validated = $validator->validated();
            $course = UserCourse::create($validated);
            $course->save();

            return ['status' => 200, 'message' => 'Data berhasil ditambahkan'];
        } catch (\Throwable $th) {
            return ['status' => 500, 'message' => 'Data gagal ditambahkan'];
        }
    }

    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            try {
                $course = UserCourse::with('user', 'course')->find($id);
                $users = User::All();
                $courses = Course::All();

                return ['status' => 200, 'course' => $course, 'users' => $users, 'courses' => $courses];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Gagal mengambil data!'];
            }
        }
    }

    public function update(Request $request, $id)
    {
        $course = UserCourse::where('id', $id)->first();

        $rules = [
            'id_user' => 'required',
            'id_course' => 'required',
        ];

        $messages = [
            'required' => ':attribute harus diisi',
        ];

        $aliases = [
            'id_user' => 'User',
            'id_course' => 'Course',
        ];

        $validator = Validator::make($request->all(), $rules, $messages, $aliases);

        if ($validator->fails()) {
            return ['status' => 422, 'validation' => $validator->getMessageBag()];
        }

        //check data sudah ada
        $check = UserCourse::where('id_user', $request->id_user)->where('id_course', $request->id_course)->get();
        if($check->count() != 0)
        {
            return ['status' => 500, 'message' => 'Data gagal ditambahkan, user dan kursus sudah ada!'];
        }

        try {
            $validated = $validator->validated();
            $course->update($validated);

            return ['status' => 200, 'message' => 'Data berhasil diubah'];
        } catch (\Throwable $th) {
            return ['status' => 500, 'message' => 'Data gagal diubah'];
        }
    }

    public function destroy(UserCourse $id)
    {
        if (request()->ajax()) {
            try {
                $id->delete();
                return ['status' => 200, 'message' => 'Data berhasil dihapus'];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Data gagal dihapus'];
            }
        }
    }

    public function getAll()
    {
        if (request()->ajax()) {
            try {
                $users = User::All();
                $courses = Course::All();

                return ['status' => 200, 'users' => $users, 'courses' => $courses];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Gagal mengambil data!'];
            }
        }
    }
}
