<?php

namespace App\Http\Controllers;

use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (request()->ajax()) {
            $data = UserRole::orderBy('id', 'asc')->get();
            return DataTables::of($data)
              ->addIndexColumn()
              ->addColumn('action', function ($role) {
                $state = [
                    'disable' => $role->name == "administrator" ? 'disabled' : '',
                ];

                $button =  "
                    <div class='d-flex'>
                        <button class='btn btn-warning btn-sm mx-1 edit-data' data-id='$role->id' type='button' $state[disable]><i class='bi bi-pencil'></i></button>
                        <button class='btn btn-danger btn-sm delete-data' data-id='$role->id' type='button' $state[disable]><i class='bi bi-trash'></i></button>
                    </div>
                    ";

                return $button;
                
              })
              ->rawColumns(['action'])
              ->make('true');
        }

        return view('dashboard.role.index');
    }

    public function store(Request $request)
    {
        if ($request->ajax()) {
            $rules = [
                'name' => 'required',
            ];

            $messages = [
                'required' => ':attribute harus diisi',
            ];

            $aliases = [
                'name' => 'Nama role',
            ];

            $validator = Validator::make($request->all(), $rules, $messages, $aliases);

            if ($validator->fails()) {
                return ['status' => 422, 'validation' => $validator->getMessageBag()];
            }

            try {

                DB::transaction(function () use ($validator) {
                    $validated = $validator->validated();
                    $role = new UserRole;
                    $role->fill($validated);
                    $role->save();
                });

                return ['status' => 200, 'message' => 'Data berhasil ditambahkan'];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Data gagal ditambahkan' . $th->getMessage()];
            }
        }
    }

    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            try {
                $role = UserRole::find($id);
                return ['status' => 200, 'role' => $role];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Gagal mengambil data!'];
            }
        }
    }

    public function update(Request $request, $id)
    {
        $role = UserRole::where('id', $id)->first();

        $rules = [
            'name' => 'required',
        ];

        $messages = [
            'required' => ':attribute harus diisi',
        ];

        $aliases = [
            'name' => 'Nama role',
        ];

        $validator = Validator::make($request->all(), $rules, $messages, $aliases);

        if ($validator->fails()) {
            return ['status' => 422, 'validation' => $validator->getMessageBag()];
        }

        try {
            $validated = $validator->validated();
            $role->update($validated);
            return ['status' => 200, 'message' => 'Data berhasil diubah'];
        } catch (\Throwable $th) {
            return ['status' => 500, 'message' => 'Data gagal diubah'];
        }
    }

    public function destroy(UserRole $id)
    {
        if (request()->ajax()) {
            //check relation
            if($id->user) {
                return ['status' => 500, 'message' => 'Data gagal dihapus, data mempunyai relasi'];
            } else {
                try {
                  $id->delete();
                  return ['status' => 200, 'message' => 'Data berhasil dihapus'];
                } catch (\Throwable $th) {
                  return ['status' => 500, 'message' => 'Data gagal dihapus'];
                }
            }
        }
    }

    public function getAll()
    {
        if (request()->ajax()) {
            try {
                $roles = UserRole::All();
                return ['status' => 200, 'roles' => $roles];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Gagal mengambil data!'];
            }
        }
    }
}
