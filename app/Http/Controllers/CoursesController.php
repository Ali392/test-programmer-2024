<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\UserCourse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class CoursesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (request()->ajax()) {
            $data = Course::orderBy('id', 'asc')->get();
            return DataTables::of($data)
              ->addIndexColumn()
              ->addColumn('action', function ($course) {
                    return '
                        <div class="d-flex">
                          <button class="btn btn-warning btn-sm mx-1 edit-data" data-id="' . $course->id . '" type="button"><i class="bi bi-pencil"></i></button>
                          <button class="btn btn-danger btn-sm delete-data" data-id="' . $course->id . '" type="button"><i class="bi bi-trash"></i></button>
                        </div>
                        ';
                
              })
              ->rawColumns(['action'])
              ->make('true');
        }

        return view('dashboard.courses.index');
    }

    public function store(Request $request)
    {
        $rules = [
            'course' => 'required',
            'mentor' => 'required',
            'title' => 'required',
        ];

        $messages = [
            'required' => ':attribute harus diisi',
        ];

        $aliases = [
            'course' => 'Nama Course',
            'mentor' => 'Nama Mentor',
            'title' => 'Title Mentor',
        ];

        $validator = Validator::make($request->all(), $rules, $messages, $aliases);

        if ($validator->fails()) {
            return ['status' => 422, 'validation' => $validator->getMessageBag()];
        }

        try {
            $validated = $validator->validated();
            $course = Course::create($validated);
            $course->save();

            return ['status' => 200, 'message' => 'Data berhasil ditambahkan'];
        } catch (\Throwable $th) {
            return ['status' => 500, 'message' => 'Data gagal ditambahkan'];
        }
    }

    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            try {
                $course = Course::find($id);
                return ['status' => 200, 'course' => $course,];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Gagal mengambil data!'];
            }
        }
    }

    public function update(Request $request, $id)
    {
        $course = Course::where('id', $id)->first();

        $rules = [
            'course' => 'required',
            'mentor' => 'required',
            'title' => 'required',
        ];

        $messages = [
            'required' => ':attribute harus diisi',
        ];

        $aliases = [
            'course' => 'Nama Course',
            'mentor' => 'Nama Mentor',
            'title' => 'Title Mentor',
        ];

        $validator = Validator::make($request->all(), $rules, $messages, $aliases);

        if ($validator->fails()) {
            return ['status' => 422, 'validation' => $validator->getMessageBag()];
        }

        try {
            $validated = $validator->validated();
            $course->update($validated);

            return ['status' => 200, 'message' => 'Data berhasil diubah'];
        } catch (\Throwable $th) {
            return ['status' => 500, 'message' => 'Data gagal diubah'];
        }
    }

    public function destroy(Course $id)
    {
        if (request()->ajax()) {

            //check if active user & course
            $userCourse = UserCourse::where('id_course', $id->id)->get();
            if($userCourse->count() != 0)
            {
                return ['status' => 500, 'message' => 'Data gagal dihapus, masih ada user aktif yang mengikuti kursus!'];
            }

            try {
                $id->delete();
                return ['status' => 200, 'message' => 'Data berhasil dihapus'];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Data gagal dihapus'];
            }
        }
    }
}
