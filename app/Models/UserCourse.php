<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCourse extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    // ===== Relations =====
    public function user() {
        return $this->belongsTo(User::class, 'id_user', 'id', 'id');
    }

    public function course() {
        return $this->belongsTo(Course::class, 'id_course', 'id', 'id');
    }
}
