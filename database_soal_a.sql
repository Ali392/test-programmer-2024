CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `users` (`id`, `username`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Andi', 'andi@andi.com', '12345', '2019-01-28 05:15:29', '2019-01-28 05:15:29'),
(2, 'Budi', 'budi@budi.com', '67890', '2019-01-28 05:15:29', '2019-01-28 05:15:29'),
(3, 'Caca', 'caca@caca.com', 'abcde', '2019-01-28 05:15:29', '2019-01-28 05:15:29'),
(4, 'Deni', 'deni@deni.com', 'fghij', '2019-01-28 05:15:29', '2019-01-28 05:15:29'),
(5, 'Euis', 'euis@euis.com', 'klmno', '2019-01-28 05:15:29', '2019-01-28 05:15:29'),
(6, 'Fafa', 'fafa@fafa.com', 'pqrst', '2019-01-28 05:15:29', '2019-01-28 05:15:29');

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `course` varchar(50) NOT NULL,
  `mentor` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `courses` (`course`, `mentor`, `title`) VALUES 
('C++', 'Ari', 'Dr.'),
('C#', 'Ari', 'Dr.'),
('C#', 'Ari', 'Dr.'),
('CSS', 'Cania', 'S.Kom'),
('HTML', 'Cania', 'S.Kom'),
('Javascript', 'Cania', 'S.Kom'),
('Phyton', 'Barry', 'S.T'),
('Microphyton', 'Barry', 'S.T'),
('Java', 'Darren', 'M.T'),
('Ruby', 'Darren', 'M.T');

CREATE TABLE `userCourse` (
    `id_user` int(11) DEFAULT NULL,
    `id_course` int(11) DEFAULT NULL,
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `userCourse` (`id_user`, `id_course`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 4),
(2, 5),
(2, 6),
(3, 7),
(3, 8),
(3, 9),
(4, 1),
(4, 3),
(4, 5),
(5, 2),
(5, 4),
(5, 6),
(6, 7),
(6, 8),
(6, 9);