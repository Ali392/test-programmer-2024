@extends('layouts.dashboard-app')
@section('content')
<main id="main" class="main content">

    <div class="pagetitle">
      <h1>List Courses</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
          <li class="breadcrumb-item active">List Courses</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col">
                <div class="d-flex justify-content-end p-2">
                    <a class="btn btn-primary" href="#" id="add-course">
                        <i class="bi bi-plus-square me-1"></i>
                        Tambah Course
                    </a>
                </div>
            </div>
        </div>

      <div class="row">
        <div class="col">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">List Courses</h5>

              <!-- Default Table -->
              <table class="table" id="datatable-courses">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Course</th>
                    <th scope="col">Mentor</th>
                    <th scope="col">Title</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
              <!-- End Default Table Example -->
            </div>
          </div>

        </div>
      </div>
    </section>

  </main>
@endsection

@section('script')
<script src="{{ asset('assets/module/courses/app.js') }}" type="module"></script>
@endsection