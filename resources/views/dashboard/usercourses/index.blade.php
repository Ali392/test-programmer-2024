@extends('layouts.dashboard-app')
@section('content')
<main id="main" class="main content">

    <div class="pagetitle">
      <h1>List User Courses</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
          <li class="breadcrumb-item active">List User Courses</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col">
                <div class="d-flex justify-content-end p-2">
                    <a class="btn btn-primary" href="#" id="add-usercourse">
                        <i class="bi bi-plus-square me-1"></i>
                        Tambah User Course
                    </a>
                </div>
            </div>
        </div>

      <div class="row">
        <div class="col">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">List User Courses</h5>

              <!-- Default Table -->
              <table class="table" id="datatable-usercourses">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">User</th>
                    <th scope="col">Course</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
              <!-- End Default Table Example -->
            </div>
          </div>

        </div>
      </div>
    </section>

  </main>
@endsection

@section('script')
<script src="{{ asset('assets/module/usercourses/app.js') }}" type="module"></script>
@endsection