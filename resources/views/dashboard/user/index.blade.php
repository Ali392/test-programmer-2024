@extends('layouts.dashboard-app')
@section('content')
<main id="main" class="main content">

    <div class="pagetitle">
      <h1>Akun User</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
          <li class="breadcrumb-item active">Akun User</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
			<div class="row">
				<div class="col">
                    <div class="d-flex justify-content-end p-2">
                        <a class="btn btn-info me-2" href="{{ route('user.export') }}" id="export-user">
                            <i class="bi bi-box-arrow-in-down me-1"></i>
                            Export excel
                        </a>

                        <a class="btn btn-primary" href="#" id="add-user">
                            <i class="bi bi-plus-square me-1"></i>
                            Tambah user
                        </a>
                    </div>
				</div>
			</div>

      <div class="row">
        <div class="col">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Akun User</h5>

              <!-- Default Table -->
              <table class="table" id="datatable-user">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama User</th>
                    <th scope="col">Email</th>
                    <th scope="col">Role</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
              <!-- End Default Table Example -->
            </div>
          </div>

        </div>
      </div>
    </section>

  </main>
@endsection

@section('script')
<script src="{{ asset('assets/module/user/app.js') }}" type="module"></script>
@endsection