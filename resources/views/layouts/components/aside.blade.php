<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

      <li class="nav-item">
        <a class="nav-link {{ Request::is('*home*') ? '' : 'collapsed' }}" href="{{ route('home') }}">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->

      @if(auth()->user()->role_id == 1)
      <li class="nav-heading">Setting</li>

      <li class="nav-item">
        <a class="nav-link {{ Request::is('*role*') ? '' : 'collapsed' }}" href="{{ route('role.index') }}">
          <i class="bi bi-people"></i>
          <span>Role User</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link {{ Request::is('*user*') ? '' : 'collapsed' }}" href="{{ route('user.index') }}">
          <i class="bi bi-people"></i>
          <span>Akun User</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link {{ Request::is('*courses*') ? '' : 'collapsed' }}" href="{{ route('courses.index') }}">
          <i class="bi bi-people"></i>
          <span>Courses</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link {{ Request::is('*usercourses*') ? '' : 'collapsed' }}" href="{{ route('usercourses.index') }}">
          <i class="bi bi-people"></i>
          <span>User & Courses</span>
        </a>
      </li>
      @endif

    </ul>

  </aside>