<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CoursesController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserCoursesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Role Routes
Route::name('role.')->group(function () {
    Route::get('/role', [RoleController::class, 'index'])->name('index');
    Route::post('/role/store', [RoleController::class, 'store'])->name('store');
    Route::get('/role/{id}/edit', [RoleController::class, 'edit'])->name('edit');
    Route::patch('/role/update/{id}', [RoleController::class, 'update'])->name('update');
    Route::delete('/role/delete/{id}', [RoleController::class, 'destroy'])->name('delete');
    Route::get('/role/getAll', [RoleController::class, 'getAll']);
});

//User Routes
Route::name('user.')->group(function () {
    Route::get('/user', [UserController::class, 'index'])->name('index');
    Route::post('/user/store', [UserController::class, 'store'])->name('store');
    Route::get('/user/{id}/edit', [UserController::class, 'edit'])->name('edit');
    Route::patch('/user/update/{id}', [UserController::class, 'update'])->name('update');
    Route::delete('/user/delete/{id}', [UserController::class, 'destroy'])->name('delete');
    Route::get('/user/export', [UserController::class, 'export'])->name('export');
});

Route::name('courses.')->group(function () {
    Route::get('/courses', [CoursesController::class, 'index'])->name('index');
    Route::post('/courses/store', [CoursesController::class, 'store'])->name('store');
    Route::get('/courses/{id}/edit', [CoursesController::class, 'edit'])->name('edit');
    Route::patch('/courses/update/{id}', [CoursesController::class, 'update'])->name('update');
    Route::delete('/courses/delete/{id}', [CoursesController::class, 'destroy'])->name('delete');
});

Route::name('usercourses.')->group(function () {
    Route::get('/usercourses', [UserCoursesController::class, 'index'])->name('index');
    Route::post('/usercourses/store', [UserCoursesController::class, 'store'])->name('store');
    Route::get('/usercourses/{id}/edit', [UserCoursesController::class, 'edit'])->name('edit');
    Route::patch('/usercourses/update/{id}', [UserCoursesController::class, 'update'])->name('update');
    Route::delete('/usercourses/delete/{id}', [UserCoursesController::class, 'destroy'])->name('delete');
    Route::get('/usercourses/getAll', [UserCoursesController::class, 'getAll'])->name('getAll');
});